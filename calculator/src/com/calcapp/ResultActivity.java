package com.calcapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ResultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.result_layout);
			
			Intent intent = getIntent();
			
			TextView resultTextView = (TextView)findViewById(R.id.resultTextView);
            resultTextView.setText(intent.getStringExtra(MainActivity.RESULT_ID));
			
        }catch (Exception error){
            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
