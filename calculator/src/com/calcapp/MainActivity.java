package com.calcapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Class present main activity.
 *
 * This class perform calculation arithmetic
 * expression and sends result to another
 * activity.
 *
 * @author Dudakov I.A.
 */
public class MainActivity extends Activity {

    static final String RESULT_ID = "result";

    private static final String SUB_OPERATOR = "+";
    private static final String DIF_OPERATOR = "-";
    private static final String DIV_OPERATOR = "/";
    private static final String MUL_OPERATOR = "*";

    private EditText aField;
    private EditText bField;
    private EditText opField;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        aField = (EditText) findViewById(R.id.aField);
        bField = (EditText) findViewById(R.id.bField);
        opField = (EditText) findViewById(R.id.opField);
    }

    /**
     * Method perform calculation arithmetic
     * expression with a and b variables, in
     * accordance with the specified operator.
     *
     * A possible operators:
     * +: sum a and b.
     * -: difference b from a.
     * /: division a on b.
     * *: multiplication a and b.
     *
     * @param a - first value.
     * @param b - second value.
     * @param operator - indicating the desired operation.
     * @return - result calculating a and b.
     */
    private float calculate(float a, float b, String operator){
        switch (operator){
            case SUB_OPERATOR:
                return a + b;

            case DIF_OPERATOR:
                return a - b;

            case DIV_OPERATOR:
                return a / b;

            case MUL_OPERATOR:
                return a * b;

            default:
                return 0.0f;
        }
    }

    /**
     * Method, which the assigned as callback
     * in calculation button and is called when
     * performed press on button.
     */
    public void onCalculateButton(View view){
        try {
            String aStr = aField.getText().toString();
            String bStr = bField.getText().toString();
            String opStr = opField.getText().toString();

            float a = Float.parseFloat(aStr);
            float b = Float.parseFloat(bStr);

            float result = calculate(a, b, opStr);

            String msg = String.format("%s\n%s\n%s\n=\n%.8f", a, opStr, b, result);

            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            intent.putExtra(RESULT_ID, msg);
            startActivity(intent);

        }catch (Exception error){
            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
